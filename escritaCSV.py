import os, shutil, subprocess, csv

os.system("title "+"Buscar grupos de conta de usuário do AD")

### Cria o arquivo PS para pesquisar os grupos do usuário alvo
def criaPSparaPesquisarGrupos(nif):
    comando = ('Import-Module activedirectory \n')
    comando1 = ('(Get-ADUser -Identity ' +str(nif)+ ' -Properties MemberOf | Select-Object MemberOf).MemberOf > C:\\temp\\user.csv')
    with open('C:\\temp\\user.ps1', 'w', newline="\n", encoding='utf8') as dados:
        dados.write(comando)
        dados.write(comando1)

### Lê o arquivo .csv que esta na pasta temp (contém os grupos do usuário alvo)
def lerGrupos():
    with open('C:\\temp\\user.csv', mode='r', encoding='utf16', newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        grupo = []
        for linha in reader:
            grupo.append(linha[0])
        return grupo

### Cria um novo arquivo .csv com os grupos separados
def gravarGruposEmCSV(dados):
    with open('C:\\temp\\user.csv', mode='w', encoding='utf16') as csvfile:
        cont = []
        for linha in dados:
            novalinha = linha.replace('CN=', '')
            cont.append(novalinha+';'+'\n')
        csvfile.writelines(cont)

### Chama o PS pelo cmd
def chamarPS():
    subprocess.call(['powershell', '-Command', 'C:\\temp\\user.ps1'], shell = True)

### MAIN
print('\n')
nif = input('   Informe o NIF do usuário: ')

while True:
    criaPSparaPesquisarGrupos(nif)
    chamarPS()
    dados = lerGrupos()
    gravarGruposEmCSV(dados)
    processo = subprocess.Popen([os.path.relpath('C:\\Program Files\\Microsoft Office\\root\\Office16\\EXCEL.EXE'), " /x" + os.path.relpath('C:\\temp\\user.csv')])

    os.system('cls')
    
    print('\n')
    nif = input('   Informe o NIF do usuário: ')
    
    processo.terminate()