import csv
from bottle import run, jinja2_view, route, request, response

def pesquisa():
    with open('user.csv', mode='r', encoding='UTF16', newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        grupo = []
        unidade = []
        for l in reader:
            grupo.append(l[0].replace('CN=', ''))
            unidade.append(l[1].replace('OU=', ''))
    return grupo, unidade

@route('/', method='GET')
@jinja2_view('paginas/view.html')
def index():
    dados = pesquisa()
    grupo = dados[0]
    unidade = dados[1]
    #grupo = ['GG_SS156_IMP_6', 'G_SCSM_CS_OP', 'G_FORTINET_RLT_INTERNET_DTI', 'G_SEDE_ACESSO_INTERNET_DTI_RESTRITO', 'G_VPN_SSL_JUNOS_PULSE']
    return dict(string='Agora tem que ir', grupo = grupo, unidade = unidade)

run(port='8080')
